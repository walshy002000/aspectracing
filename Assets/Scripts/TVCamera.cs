﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVCamera : MonoBehaviour
{
    GameObject go;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (go == null)
        {
            go = GameObject.Find("Main Camera");
        }

        transform.rotation = go.transform.rotation;
    }
}
