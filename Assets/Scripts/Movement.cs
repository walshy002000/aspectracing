﻿using UnityEngine;
using System.Collections;
 
public class Movement : MonoBehaviour
{
    CharacterController characterController;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float verticalSpeed = 20.0f;
    public float horizontalSpeed = 20.0f;

    private Vector3 moveDirection = Vector3.zero;

    GameObject go;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (characterController.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        Vector3 movement = Vector3.zero;
        float v = Input.GetAxis("Vertical");
        Debug.Log(v);
        if (v > 0.5)
        {
            if (go == null)
            {
                go = GameObject.Find("cargarage_obj");
            }
            //go.SetActive(!go.active);
        }
        float h = Input.GetAxis("Horizontal");
        movement += transform.GetChild(0).forward * v * speed * Time.deltaTime;
        movement += transform.GetChild(0).right * h * speed * Time.deltaTime;
        movement += Physics.gravity;
        characterController.Move(movement);

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        //moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        //characterController.Move(moveDirection * Time.deltaTime);

        //gameObject.transform.localPosition += moveDirection;


    }
}